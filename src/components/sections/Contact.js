import React from 'react'
import Button from '../Button';

const Contact = () => {
    return (
        <section id="contact"className="container mx-auto my-20 py-24 bg-blue-100 rounded-lg text-center text-xl font-light">
            <h2 className="text-3xl lg:text-5xl font-semibold">Contact us</h2>
            <p>
            Get in touch and let's find out how floating treatment wetlands can help you.
            </p>
            <p>Hugh Forsyth </p>
            <p>Phone: 021 304 712</p>
            <p>Email: info@biostream.co.nz</p>
      </section>
    )
}

export default Contact