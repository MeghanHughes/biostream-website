import  React from 'react'
import SplitSection from '../SplitSection';
import FTW from '../../images/FWT.svg'
import DownloadButton from '../DownloadButton';

const AboutFTW  = () => {
    return (
        <div id="aboutFTW" className="pt-40">
            <div className="bg-gradient-to-t from-blue-100 to-transparent">
                <div className="container mx-auto text-center">
                    <h2 className=" text-3xl lg:text-5xl font-semibold">About Floating Treatment Wetlands</h2>
                </div>
                <div className="lg:flex justify-center">
                    <img className="max-w-5xl pt-5" src={FTW} alt='ftw' />
                </div>
                <SplitSection
                    primarySlot={
                        <div className="lg:pr-32 xl:pr-48">
                        <h3 className="text-3xl font-semibold leading-tight">What are FTW?</h3>
                        <p className="mt-3 text-xl font-light leading-relaxed">
                        Floating Treatment Wetlands (FTW) replicate the water filtering function of a natural biological wetland by providing a habitat for native plants that is free from flood and siltation effect. This significantly increases the removal of dissolved chemical, microbial, siltation and other pollutants within wastewater. 
                        </p>
                        </div>
                    }
                    secondarySlot= {
                        <div className="lg:pr-32 xl:pr-48">
                            <h3 className="text-3xl font-semibold leading-tight">How do FTW work?</h3>
                            <p className="mt- text-xl font-light leading-relaxed">
                                FTWs are floating pontoons planted with native wetland species whose root systems grow below. These provide an environment for mico-organisms that naturally convert suspended solids and soluble chemicals to inert nodules. The nodules are stored on the root-structure and then fall to the pond floor for later removal. The pontoons are non-toxic and long lasting with a product life of 12+ years.
                            </p>
                        </div>
                    }
                />
                <div className="lg:flex flex-col items-center -center pb-10">
                    <p>For more information about how floating treatment wetlands can help you</p>
                    <a href="download.pdf"><DownloadButton className="mt-5">Download our brochure</DownloadButton></a>
                </div>
            </div>
        </div>
    )
}

export default AboutFTW