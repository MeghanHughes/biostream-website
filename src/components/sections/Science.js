import React from 'react'
import SplitSection from '../SplitSection';
import SvgCharts from '../../svg/SvgCharts';

const Science = () => {
    return (
        <div>
            <div id="science" className="container mx-auto text-center">
                <h2 className="text-3xl lg:text-5xl font-semibold">The Science</h2>
            </div>
            <SplitSection
            primarySlot={
                <div className="lg:pr-32 xl:pr-48">
                <h3 className="text-3xl font-semibold leading-tight">Market Analysis</h3>
                <p className="mt-8 text-xl font-light leading-relaxed">
                    Our team of enthusiastic marketers will analyse and evaluate how your company stacks
                    against the closest competitors
                </p>
                </div>
            }
            secondarySlot={<SvgCharts />}
            />
            <SplitSection
            reverseOrder
            primarySlot={
                <div className="lg:pl-32 xl:pl-48">
                <h3 className="text-3xl font-semibold leading-tight">
                    Design And Plan Your Business Growth Steps
                </h3>
                <p className="mt-8 text-xl font-light leading-relaxed">
                    Once the market analysis process is completed our staff will search for opportunities
                    that are in reach
                </p>
                </div>
            }
            secondarySlot={<SvgCharts />}
            />
            <SplitSection
            primarySlot={
                <div className="lg:pr-32 xl:pr-48">
                <h3 className="text-3xl font-semibold leading-tight">
                    Search For Performance Optimization
                </h3>
                <p className="mt-8 text-xl font-light leading-relaxed">
                    With all the information in place you will be presented with an action plan that your
                    company needs to follow
                </p>
                </div>
            }
            secondarySlot={<SvgCharts />}
            />
        </div>
    )
}

export default Science