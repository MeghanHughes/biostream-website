import React from 'react'
import DownloadButton from '../DownloadButton'
import AnchorLink from 'react-anchor-link-smooth-scroll';
import Hunterville from '../../images/hunterville.jpg'

const Landing = () => {
    return (
        <section id="landing" className="pt-20 md:pt-40">
          <div className="container mx-auto px-8 lg:flex">
            <div className="text-center lg:text-left lg:w-1/2">
              <h1 className="text-4xl lg:text-5xl xl:text-6xl font-bold leading-none">
                Achieve compliance and sustainability targets with floating wetland treatment technology.
              </h1>
              <p className="text-xl lg:text-2xl mt-6 font-light">
                We will work with you to build the right solution
              </p>
              <a href="download.pdf"><DownloadButton className="mt-5">Download our brochure</DownloadButton></a>
            </div>
            <div className="lg:w-1/2">
              <img src={Hunterville} alt="Hunterville" />
            </div>
          </div>
        </section>
    )
}

export default Landing