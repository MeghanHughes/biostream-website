import  React from 'react'
import SplitSection from '../SplitSection';
import Hugh from '../../images/Hugh.jpg'

const AboutUs = () => {
    return (
        <div id="aboutUs" className="container mx-auto pt-40">
            <div>
                <h2 className="pb-10 text-3xl lg:text-5xl font-semibold text-center">About Biostream</h2>
            </div>
            <div className="lg:flex">
                <div className="flex-col lg:w-1/2 px-10">
                    <h3 className="text-3xl font-semibold leading-tight">Biostream works with clients to design and install the best Floating Treatment Wetlands solution across rural, semi-rural and municipal environments.</h3>
                    <p className="mt-3 text-xl font-light leading-relaxed">
                        Biostream was established by Hugh Forsyth in XXXX. Hugh is passionate about the economic, environmental, and regulatory benefits that floating treatment wetlands provide. With extensive experience in landscape architecture, project development and regulatory consent in both the United Kingdom and New Zealand, Hugh knows how to get to the bottom of the best solution.
                    </p>
                    <p className="mt-3 text-xl font-light leading-relaxed">
                    Biostream resells on behalf of <a href='http://waterclean.co.nz/' rel-='Waterclean'>Waterclean</a> who have developed and installed Floating Treatment Wetlands in New Zealand, Australia, and Singapore for over 15 years. Waterclean manufactures and develops the product and stands behind its longevity and track record of successful water treatment. 
                    </p>
                </div>
                <div className="lg:w-1/2">
                    <img src={Hugh} alt="Hugh" />
                </div>
            </div>
        </div>
    )
}

export default AboutUs