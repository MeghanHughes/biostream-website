import React from 'react'
import Card from '../Card';

const Benefits = () => {
    return (
        <section id="features" className="lg:pt-40">
        <div className="container mx-auto text-center">
          <h2 className="text-3xl lg:text-5xl font-semibold">Main Benefits</h2>
          <div className="flex flex-col sm:flex-row sm:-mx-3 mt-12">
            <div className="flex-1 px-3">
              <Card className="mb-8">
                <p className="font-semibold text-xl">Achieve compliance</p>
                <p className="mt-4">
                FTWs have been successfully applied to small town and municipal waste treatment and water filtering systems in New Zealand since 2008. 
                </p>
              </Card>
            </div>
            <div className="flex-1 px-3">
              <Card className="mb-8">
                <p className="font-semibold text-xl">Cost effective</p>
                <p className="mt-4">
                FTWs are a long-term solution for small to medium scale water treatment requirements that is easy to maintain, cost effective, and safe.
                </p>
              </Card>
            </div>
            <div className="flex-1 px-3">
              <Card className="mb-8">
                <p className="font-semibold text-xl">Self-managing solution</p>
                <p className="mt-4">
                FTWs can be installed into existing or new pond environments and require little additional infrastructure or ongoing management to become operational. 
                </p>
              </Card>
            </div>
          </div>
        </div>
      </section>
    )
}

export default Benefits