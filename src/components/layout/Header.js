import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import LogoIcon from '../../svg/LogoIcon';
import Button from '../Button';
import DownloadButton from '../DownloadButton';

const Header = () => (
  <header className="sticky top-0 bg-white shadow">
    <div className="container flex flex-col sm:flex-row justify-between items-center mx-auto py-4 px-8">
      <div className="flex items-center text-2xl text-blue-900">
        <AnchorLink className="px-4" href="#landing">
          Biostream
        </ AnchorLink>
      </div>
      <div className="flex mt-4 sm:mt-0 text-blue-900">
        <AnchorLink className="px-4" href="#aboutFTW">
          About Floating Treatment Wetlands
        </AnchorLink>
        <AnchorLink className="px-4" href="#aboutUs">
          About us
        </AnchorLink>
        <AnchorLink className="px-4" href="#contact">
          Contact us
        </AnchorLink>
      </div>
      <div className="hidden md:block">
        <a href="download.pdf"><DownloadButton className="text-sm">Download brochure</DownloadButton></a>
      </div>
    </div>
  </header>
);

export default Header;
