import React from 'react';
import DownloadButton from '../DownloadButton';

const Footer = () => (
  <footer className="container mx-auto py-16 px-3 mt-48 mb-8 text-gray-800">
    <div className="flex -mx-3">
      <div className="flex-1 px-3">
        <h2 className="text-lg font-semibold">About Us</h2>
        <p className="mt-5">Biostream is based in Dunedin, but can does work all over Aotearoa/New Zealand.</p>
      </div>
      <div className="flex-1 px-3">
        <h2 className="text-lg font-semibold">Contact</h2>
        <p>Hugh Forsyth </p>
            <p>Phone: 021 304 712</p>
            <p>Email: info@biostream.co.nz</p>  
      </div>
      <div className="flex-1 px-3">
        <h2 className="text-lg font-semibold">Further information</h2>
       <p>For more information about FTW and how it can work for you <a href='download.pdf'> download our brochure. </a></p>
      </div>
    </div>
  </footer>
);

export default Footer;
