import React from 'react';

const sizes = {
  default: `py-3 px-8`,
  lg: `py-4 px-12`,
  xl: `py-5 px-16 text-lg`
};

const DownloadButton = ({ children, className = '', size }) => {
  return (
    <button
      type="button"
      className={`
        ${sizes[size] || sizes.default}
        ${className}
        bg-amber-300
        hover:bg-amber-500
        rounded
        text-black
    `}
    >
      {children}
    </button>
  );
};

export default DownloadButton;