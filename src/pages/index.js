import React from 'react';
import Landing from '../components/sections/Landing';
import Benefits from '../components/sections/Benefits'
import Science from '../components/sections/Science'
import Statistics from '../components/sections/Statistics';
import Solution from '../components/sections/Solution';
import AboutFTW from '../components/sections/AboutFTW'
import Contact from '../components/sections/Contact';
import Layout from '../components/layout/Layout';
import AboutUs from '../components/sections/AboutUs';

const Index = () => (
  <Layout>
    <Landing />
    <Benefits />
    <AboutFTW />
    {/* <Solution /> */}
    {/* <Statistics />   */}
    {/* <Science /> */}
    <AboutUs />
    <Contact />
  </Layout>
);

export default Index;
