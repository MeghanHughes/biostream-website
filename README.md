# Biostream website.

This website was built using [gatsby-theme-codebushi](https://github.com/codebushi/gatsby-theme-codebushi), a theme which uses [Tailwind CSS](https://tailwindcss.com/).

The content is owned in its entirety by Biosteam, and the project is managed by @meghanhughes.

## Preview

The development website is deployed on [https://meghanhughes.gitlab.io/biostream-website/](https://meghanhughes.gitlab.io/biostream-website/)

## Install the project

To install a local copy of this project, clone this repository.
<br/>

## Install Gatsby

If you don't already have Gatsby installed, before you clone the repo or before you run the start script, follow this [installation guide](https://www.gatsbyjs.com/docs/tutorial/part-0/#installation-guide).

## Run script

Once you have Gatsby installed and cloned the repo, run `gatsby develop` in the terminal to start the dev site.

## Gatsby Notes

These libraries do not yet fully support the upgrade to Gatsby 3.x:
* gatsby-plugin-react-helmet
* gatsby-plugin-emotion
* gatsby-plugin-postcss
* gatsby-theme-codebushi
