const colors = require('tailwindcss/colors')

module.exports = {
  theme: {
    extend: {
      colors: {
        blue: colors.blue,
        emerald: colors.emerald,
        amber: colors.amber,
        primary: {
          lighter: 'hsl(207, 73%, 52%)',
          default: 'hsl(207, 73%, 57%)',
          darker: 'hsl(207, 73%, 44%)'
        }
      }
    }
  },
  variants: {},
  plugins: []
};
